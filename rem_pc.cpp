#include <iostream>
#include <pthread.h>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <math.h>
#include <chrono>

using namespace std;

#define NTHREADS 50

#define SIZE  500

//cria a estrutura de dados para armazenar os argumentos da thread
typedef struct {
	int idThread, nThreads;
	int linhas;
} t_Args;

sf::Image imagem;

void geraLinhas(int i){
    int r,g,b;
    for (int j = 0; j < SIZE; j++) {
        r = (int) sqrt(pow(i - 0, 2) + pow(j - 0, 2));
        g = (int) sqrt(pow(i - 0, 2) + pow(j - SIZE, 2));
        b = (int) sqrt(pow(i - SIZE, 2) + pow(j - SIZE, 2));

        imagem.setPixel(i, j, sf::Color(r,g,b));
    }
}

void *FuncaoThread(void *arg){
    
    t_Args *args = (t_Args*) arg;
    int fracao = args->linhas/args->nThreads;
    int posInit = (args->idThread-1)*fracao;
    int posF = posInit + fracao-1;

	for(int i=posInit;i<=posF;i++){
        geraLinhas(i);
    }
    free(arg);
    pthread_exit(NULL);
    return 0;
}
int main() {
    
    pthread_t tid_sistema[NTHREADS];
    int t;
    t_Args *arg;

    imagem.create(SIZE, SIZE, sf::Color(0,0,0));
    sf::RenderWindow window;
    window.create(sf::VideoMode(SIZE, SIZE), "Rembrandt");
    window.setFramerateLimit(60);
    
    auto start = chrono::high_resolution_clock::now();
    for (t = 0; t < NTHREADS; t++)
    {
        arg = (t_Args*) malloc(sizeof(t_Args));
        if (arg == NULL) {
			printf("--ERRO: malloc()\n"); 
			return -1;
		}
        arg->idThread = t+1;
        arg->nThreads = NTHREADS;
        arg->linhas = SIZE;
        if (pthread_create(&tid_sistema[t], NULL, FuncaoThread, (void*) arg)) {
            printf("--ERRO: pthread_create()\n"); 
            return -1;
		}
    }
    
    //--espera todas as threads terminarem
	for (t=0; t<NTHREADS; t++) {
		if (pthread_join(tid_sistema[t], NULL)) {
			printf("--ERRO: pthread_join() \n"); 
			return -1; 
		} 
	}
    
	chrono::duration<double>tempo = chrono::high_resolution_clock::now()-start;
    cout << "\nTempo de Processamento: " << tempo.count() << "\n";
    
    sf::Texture texture;
    texture.loadFromImage(imagem);
    sf::Sprite sprite(texture);
    // run the program as long as the window is open
    while (window.isOpen())
    {
        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (window.pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
                window.close();
        }
        
        window.draw(sprite);
        
        window.display();
    }
    
    
    return 1;
}




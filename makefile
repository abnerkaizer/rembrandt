#!/usr/bin/zsh

CC=g++
CFLAGS= -c -O2 -mtune=native -Wall -Wextra -I /usr/include
LIBS= -L /usr/lib -lsfml-graphics -lsfml-window -lsfml-system -pthread
MEDIUM = rem_pc.o
TARGET= rem
all:
	$(CC) $(CFLAGS) rem_pc.cpp
	$(CC) $(MEDIUM) -o $(TARGET) $(LIBS)
clean:
	rm $(TARGET)